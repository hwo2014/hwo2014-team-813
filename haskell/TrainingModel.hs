{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleInstances #-}

module TrainingModel where
    
import GameInitModel

type Throttle = Float -- Actually Throttle should be 0.0-1.0 but can't do that!
type Ticks    = Int
type Angle    = Float
    
-- We need to know, for a segment, the minimum and maximum time (in ticks) that it took for the car to
-- get through it.  However, the time is dependent on the throttle, hence we keep these as a pair
data TimingInfo = TimingInfo Throttle Ticks deriving (Show)
                
-- The TimingInfo can now be used to build a training data, which is the bot trying to learn the optimum
-- throttle setting through a particular segment.
data TrainingInfo = TrainingInfo {
  trainingPiece :: Piece,
  minimumResult :: TimingInfo,
  maximumResult :: TimingInfo
} deriving (Show)
                          
minimumThrottle :: TrainingInfo -> Throttle
minimumThrottle TrainingInfo{minimumResult=(TimingInfo throttle _)} = throttle

maximumThrottle :: TrainingInfo -> Throttle
maximumThrottle TrainingInfo{maximumResult=(TimingInfo throttle _)} = throttle
           
-- Given the TrainingInfo for a segment we can suggest the next throttle setting to try.
suggestThrottle :: TrainingInfo -> Throttle
suggestThrottle TrainingInfo{minimumResult=(TimingInfo l _),maximumResult=(TimingInfo h _)} = 
  (l + h) / 2
                                                                                              
-- Given the TrainingInfo for a segment, if we have crashed we can adjust the information such that it
-- will go slower next time.
crashed :: TrainingInfo -> TrainingInfo
crashed info@TrainingInfo{minimumResult=l} = 
  info {minimumResult=l,maximumResult=h}
  where ct = suggestThrottle info
        h  = TimingInfo ct 9999
            
-- Given the TrainingInfo for a segment, if we have passed through the segment we can adjust it such that
-- we will try a different tactic next time.
passed :: Ticks -> TrainingInfo -> TrainingInfo
passed ticks info@TrainingInfo{minimumResult=(TimingInfo lt lc),maximumResult=(TimingInfo ht hc)} =
  let
    pt = suggestThrottle info
    pi = TimingInfo pt ticks
  in if ticks <= lc && pt > lt then
    info {minimumResult=pi}
  else if pt > ht then
    info {maximumResult=pi}
  else
    info
           
-- Initially we can generate some reasonable TrainingInfo for each type of segment.  On a straight it is
-- assumed that the car can go almost flat out, but in corners we have to be a bit more cautious.
guess :: Piece -> TrainingInfo

guess piece@Piece{pieceLength=Just(_)} =
  TrainingInfo{trainingPiece=piece,minimumResult=l,maximumResult=h}
  where l = TimingInfo 0.8 9999
        h = TimingInfo 1.0 9999

guess piece@Piece{pieceAngle=Just(a)} =
  TrainingInfo{trainingPiece=piece,minimumResult=l,maximumResult=h}
  where as     = (90.0 - abs(a)) / 90.0
        weight = 0.4
        ad     = as * (1.0 - weight)
        l      = TimingInfo (0.1+ad) 9999
        h      = TimingInfo (weight+ad) 9999
            
-- Collapsing a TrainingInfo means that the minimum & maximum values become the same.
collapse :: TrainingInfo -> TrainingInfo
collapse info@TrainingInfo{minimumResult=m} =
  info {maximumResult=m}
            
-- When training our bot we build the TrainingInfo for our track.
type TrainingData = [TrainingInfo]
                          
-- Sometimes you just need a simple average!
mean :: [Float] -> Float
mean values = 
    t / l
    where t = sum values
          l = fromIntegral (length values)
              
-- Weighted mean, with the weighting towards the start
weightedMean :: [Float] -> Float
weightedMean values =
  t / s
  where l  = length values
        z  = reverse (take l [1,2..])
        vz = zipWith (*) values z
        s  = sum z
        t  = sum vz
              
window :: Int -> [a] -> Int -> [a]
window size values index =
  map ranged indexes
  where indexes = take size [index..]
        ranged  = (\v -> values !! (v `mod` (length values)))

-- Similar in nature to map except that it takes a window size and passes a sequence of that window
-- size to the function.  Effectively mapping over a sliding window.
mapWindow :: Int -> ([a] -> b) -> [a] -> [b]
mapWindow size f values = 
  map (f . windowF) indexes
  where windowF = window size values
        indexes = take (length values) [0,1..]
                  
-- Smoothing applies around the training data, adjusting based on the average of a sliding window.
smooth :: TrainingData -> TrainingInfo
smooth trainingData@(head:_) =
  head {minimumResult=l,maximumResult=h}
  where alt = weightedMean (map minimumThrottle trainingData)
        aht = weightedMean (map maximumThrottle trainingData)
        l = TimingInfo alt 9999
        h = TimingInfo aht 9999
            
applySmoothing = mapWindow 5 smooth

initialiseTrainingData :: [Piece] -> TrainingData
initialiseTrainingData = applySmoothing . (map guess)
                         
emptyTrainingData = []
                    
like :: TrainingInfo -> (TrainingInfo -> TrainingInfo) -> TrainingInfo -> TrainingInfo
like TrainingInfo{trainingPiece=a} f info@TrainingInfo{trainingPiece=b}
  | a == b    = f info
  | otherwise = info

-- In training the system we can learn from the current state for future states.  Here we pick out the
-- training information for the current position, which gives us the criteria of the other segments
-- we can update (i.e. if we crash in a corner of 30 degrees, it's likely we will do so in others!)
train :: TrainingData -> Int -> (TrainingInfo -> TrainingInfo) -> TrainingData
train trainingData index f =
  applySmoothing updatedTrainingData
--  where currentTrainingInfo = trainingData !! index
--        updater             = like currentTrainingInfo f
--        updatedTrainingData = map updater trainingData
  where (b,(h:a)) = splitAt index trainingData
        updated   = f h
        updatedTrainingData = b ++ (updated : a)
   
