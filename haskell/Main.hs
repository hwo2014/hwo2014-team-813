{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleInstances #-}

module Main where

import System.Environment (getArgs)
import System.Exit (exitFailure)

import Network(connectTo, PortID(..))
import System.IO(hPutStrLn, hGetLine, hSetBuffering, BufferMode(..), Handle)
import Data.List
import Control.Monad
import Control.Applicative
import qualified Data.ByteString.Lazy.Char8 as L
import Data.Aeson(decode, FromJSON(..), fromJSON, parseJSON, eitherDecode, Value(..), (.:), (.:?), Result(..))

import GameInitModel
import CarPositionsModel
import CrashModel
import TrainingModel

type ClientMessage = String
           
-- Need to keep track of the segment we're in and when we entered it so that we can adjust the training data
-- we're building later.
data SegmentTiming = SegmentTiming {
  segmentIndex :: Int,
  entryTick    :: Int
} deriving (Show)
           
entered :: Int -> Ticks -> SegmentTiming
entered index tick = SegmentTiming{segmentIndex=index,entryTick=tick}

exited :: SegmentTiming -> Ticks -> Ticks
exited SegmentTiming{entryTick=s} f = f - s
                                      
-- There are two types of learning event: a crash, or successfully through a segment
data LearningEvent = Crashed Int | Passed Int Ticks
           
-- The botstate describes the bot and what it's going to do!
data BotState = BotState {
  botname        :: String,                                -- the name of the bot
  currentSegment :: Maybe SegmentTiming,                   -- where it is in the track
  trainingData   :: TrainingData,
  trainer        :: BotState -> LearningEvent -> BotState, -- when it needs to learn something!
  finisher       :: BotState -> BotState                   -- when it's finished do this!
}

-- A bot can be trained as it goes.
learner :: BotState -> LearningEvent -> BotState
learner state@BotState{trainingData=currentTrainingData} (Crashed i) =
  state {trainingData=updatedTrainingData}
  where updatedTrainingData = train currentTrainingData i crashed
                            
learner state@BotState{trainingData=currentTrainingData,currentSegment=Just(SegmentTiming{segmentIndex=currentPosition})} (Passed i tick) = 
  adjustment i tick state
  where adjustment = if i == currentPosition then neutral else update

-- The bot who has learned everything cannot be trained!
learnedEverything :: BotState -> LearningEvent -> BotState
learnedEverything state _ = state

-- A bot finishes training after the gameEnd message has been received.  At which point
-- everything it knows is transferred into everything it can be!  It can't be trained
-- any further. At this point we take the minimum values as both the minimum and maximum, 
-- hence we should be safe to keep at these throughout.
doneTraining :: BotState -> BotState
doneTraining state@BotState{trainingData=currentTrainingData} = 
    state {trainingData=raceData,trainer=learnedEverything}
    where raceData = map collapse currentTrainingData
           
-- The throttle setting for the bot is considered as a sliding window over the training
-- information.  Hence, it should slow down into corners and accelerate out of them.
throttle :: BotState -> Int -> Float
throttle BotState{trainingData=trainingData} index = 
  suggestThrottle (trainingData !! index)
--    mean throttles
--    where slidingWindow = window 5 trainingData index
--          throttles     = map suggestThrottle slidingWindow

-- Helper, nothing more!
throttles :: BotState -> [Throttle]
throttles BotState{trainingData=trainingData} = map suggestThrottle trainingData
           
-- Updates the bot state on exit from the current segment.
updateOnExit :: Int -> Ticks -> BotState -> BotState
updateOnExit _ ticks state@BotState{currentSegment=Just(SegmentTiming{segmentIndex=i,entryTick=t}),trainingData=trainingData} =
  state {trainingData=updatedTrainingData}
  where timeInSegment       = ticks - t
        update              = passed timeInSegment
        updatedTrainingData = train trainingData i update
                
-- Updates the bot state on entry to a new segment.
updateOnEntry :: Int -> Ticks -> BotState -> BotState
updateOnEntry index ticks state =
  state {currentSegment=Just(segment)}
  where segment = SegmentTiming{segmentIndex=index,entryTick=ticks}
                 
-- Updates the bot state on transition between segments
update :: Int -> Ticks -> BotState -> BotState
update position ticks = 
  (updateOnEntry position ticks) . (updateOnExit position ticks)
               
-- The bot state doesn't need to be updated
neutral :: Int -> Ticks -> BotState -> BotState
neutral _ _ state = state

-- Builds a learning bot with the given name
makeLearner :: String -> BotState
makeLearner botname = BotState{botname=botname,currentSegment=Nothing,trainingData=emptyTrainingData,trainer=learner,finisher=doneTraining}
                      
data ClientResponse = ClientResponse {
  responseState   :: BotState,
  responseMessage :: ClientMessage
}

joinMessage botname botkey = "{\"msgType\":\"join\",\"data\":{\"name\":\"" ++ botname ++ "\",\"key\":\"" ++ botkey ++ "\"}}"
throttleMessage amount     = "{\"msgType\":\"throttle\",\"data\":" ++ (show amount) ++ "}"
pingMessage                = "{\"msgType\":\"ping\",\"data\":{}}"

connectToServer server port = connectTo server (PortNumber (fromIntegral (read port :: Integer)))

main = do
  args <- getArgs
  case args of
    [server, port, botname, botkey] -> do
      run server port botname botkey
    _ -> do
      putStrLn "Usage: hwo2014bot <host> <port> <botname> <botkey>"
      exitFailure

run server port botname botkey = do
  h <- connectToServer server port
  hSetBuffering h LineBuffering
  hPutStrLn h $ joinMessage botname botkey
  handleMessages h (makeLearner botname)

handleMessages h botstate = do
  msg <- hGetLine h
  case decode (L.pack msg) of
    Just json ->
      let decoded = fromJSON json >>= decodeMessage in
      case decoded of
        Success serverMessage -> handleServerMessage h botstate serverMessage
        Error s -> fail $ "Error decoding message: " ++ s
    Nothing -> do
      fail $ "Error parsing JSON: " ++ (show msg)

data ServerMessage = 
  Join | 
  GameInit GameInitData | 
  CarPositions (Maybe Int) [CarPosition] |
  GameStart |
  GameEnd |
  Crash CrashData |
  Spawn CrashData |
  Unknown String Value
  deriving (Show)

handleServerMessage :: Handle -> BotState -> ServerMessage -> IO ()
handleServerMessage h botstate serverMessage = do
  responses <- respond botstate serverMessage
  forM_ responses $ sendResponse h
  handleMessages h (responseState (last responses))
                 
sendResponse :: Handle -> ClientResponse -> IO ()
sendResponse h ClientResponse{responseMessage=m} = hPutStrLn h m

respond :: BotState -> ServerMessage -> IO [ClientResponse]
           
-- Handling crashing, because we can learn from that!
respond botState@BotState{botname=me,currentSegment=Just(SegmentTiming{segmentIndex=i})} (Crash CrashData{_name=them})
    | me == them = 
        let
          trainerFn           = trainer botState
          learnedFromCrashing = trainerFn botState (Crashed i)
        in do
          putStrLn $ "Crashed! Slowed down through zone of " ++ (show i) ++ ": " ++ (show (throttles learnedFromCrashing))
          return $ [ClientResponse{responseState=learnedFromCrashing, responseMessage=pingMessage}]
    | otherwise  = 
        do
          putStrLn $ "Good job " ++ them ++ " crashed and not me!"
          return $ [ClientResponse{responseState=botState, responseMessage=pingMessage}]
                                
-- Handling spawning, because we need to get going again!
respond botState@BotState{botname=me} (Spawn CrashData{_name=them})
    | me == them = 
        do
          putStrLn $ "Pedal to the metal, again!"
          return $ [ClientResponse{responseState=botState, responseMessage=pingMessage}]
    | otherwise = 
        do
          putStrLn $ "Meh, " ++ them ++ " is back on the track!"
          return $ [ClientResponse{responseState=botState, responseMessage=pingMessage}]
                            
-- Handling the game initialisation, by generating our segment states!
respond botState (GameInit gameInit) = 
  let
    trainingData      = initialiseTrainingData (piecesOfGame gameInit)
    gameReadyBotState = botState {trainingData=trainingData} 
  in do
    putStrLn $ "Game initialisation"
    return $ [ClientResponse{responseState=gameReadyBotState, responseMessage=pingMessage}]

-- Handling the car position changes, by working out what we should do
respond botState@BotState{botname=botname,currentSegment=Just(_)} (CarPositions (Just tick) carPositions) =
  let
    possiblePosition  = findCar botname carPositions
  in case possiblePosition of
    Just(myCarPosition) ->
      let
        position           = pieceIndex (piecePosition myCarPosition)
        trainerFn          = trainer botState
        learnedFromDriving = trainerFn botState (Passed position tick)
        throttleInSegment  = throttle learnedFromDriving position
      in do
        return $ [ClientResponse{responseState=learnedFromDriving, responseMessage=(throttleMessage throttleInSegment)}]
               
    Nothing -> do
      putStrLn $ "What the ...!?!!"
      return $ [ClientResponse{responseState=botState, responseMessage=pingMessage}]
             
respond botState@BotState{botname=botname,currentSegment=Nothing} (CarPositions (Just tick) carPositions) =
  let
    possiblePosition  = findCar botname carPositions
  in case possiblePosition of
    Just(myCarPosition) ->
      let
        position          = pieceIndex (piecePosition myCarPosition)
        positioned        = updateOnEntry position tick botState
        throttleInSegment = throttle positioned position
      in do
        return $ [ClientResponse{responseState=positioned, responseMessage=(throttleMessage throttleInSegment)}]
               
    Nothing -> do
      putStrLn $ "What the ...!?!!"
      return $ [ClientResponse{responseState=botState, responseMessage=pingMessage}]

-- Anything else falls through to here, which is normally just a ping response
respond botState message = case message of
  Join -> do
    putStrLn "Joined"
    return $ [ClientResponse{responseState=botState, responseMessage=pingMessage}]

  GameStart ->
    let 
      position          = 0
      throttleInSegment = throttle botState position
    in do
      putStrLn "Game started!"
      return $ [ClientResponse{responseState=botState, responseMessage=(throttleMessage throttleInSegment)}]
             
  GameEnd ->
    let
      finish    = finisher botState
      raceReady = finish botState
    in do
      putStrLn $ "Game Finished: " ++ (show (throttles raceReady))
      return $ [ClientResponse{responseState=raceReady, responseMessage=pingMessage}]

  Unknown msgType msgData -> do
    putStrLn $ "Bot state: " ++ (show (throttles botState))
    putStrLn $ "Unknown message: " ++ msgType
    putStrLn $ "Unknown message body: " ++ (show msgData)
    return $ [ClientResponse{responseState=botState, responseMessage=pingMessage}]
           
  otherwise -> do
    putStrLn $ "Unmatched: " ++ (show message)
    return $ [ClientResponse{responseState=botState, responseMessage=pingMessage}]

decodeMessage :: (String, Value, Maybe Int) -> Result ServerMessage
decodeMessage (msgType, msgData, gameTick)
  | msgType == "join"         = Success Join
  | msgType == "gameInit"     = GameInit <$> (fromJSON msgData)
  | msgType == "carPositions" = CarPositions gameTick <$> (fromJSON msgData)
  | msgType == "gameStart"    = Success GameStart
  | msgType == "gameEnd"      = Success GameEnd
  | msgType == "crash"        = Crash <$> (fromJSON msgData)
  | msgType == "spawn"        = Spawn <$> (fromJSON msgData)
  | otherwise                 = Success $ Unknown msgType msgData

instance FromJSON a => FromJSON (String, a, Maybe Int) where
  parseJSON (Object v) = do
    msgType  <- v .:  "msgType"
    msgData  <- v .:  "data"
    gameTick <- v .:? "gameTick"
    return (msgType, msgData, gameTick)
  parseJSON x          = fail $ "Not an JSON object: " ++ (show x)
