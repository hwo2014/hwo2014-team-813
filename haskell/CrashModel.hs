{-# LANGUAGE DeriveDataTypeable #-}
{-# LANGUAGE OverloadedStrings #-}

module CrashModel where

import Data.Aeson ((.:), (.:?), decode, encode, (.=), object, FromJSON(..), ToJSON(..), Value(..))
import Control.Applicative ((<$>), (<*>))
    
data CrashData = CrashData {
  _name  :: String,
  _color :: String
} deriving (Show)
           
instance FromJSON CrashData where
  parseJSON (Object v) =
    CrashData <$>
    (v .: "name") <*>
    (v .: "color")
